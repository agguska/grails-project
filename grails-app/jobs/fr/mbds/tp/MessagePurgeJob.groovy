package fr.mbds.tp

import fr.mbds.tp.Message

class MessagePurgeJob {
    static triggers = {
        simple name: 'simpleTrigger', startDelay: 30000, repeatInterval: 30000, repeatCount: -1 //every 30 seconds
    }

    def execute() {
        println("purge")
        def list = Message.findAllByMessageRead(true)
        list.each {
            it.delete()
            println("Le message ${it.id} a été supprimé")
        }

    }
}
