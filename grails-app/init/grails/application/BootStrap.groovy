package grails.application

import fr.mbds.tp.*

class BootStrap {

    def init = { servletContext ->
        def adminRole = new Role(authority: "ROLE_ADMIN").save(flush:true, failOnError: true)
        def userRole = new Role(authority: "ROLE_USER").save(flush:true, failOnError: true)

        def admin = new User(username: 'admin',password: "admin", confirmPassword: "admin").save(flush:true, failOnError: true)
        def user1 = new User(username: 'Agnieszka',password: "player1", confirmPassword: "player1").save(flush:true, failOnError: true)
        def user2 = new User(username: 'Selma',password: "player2", confirmPassword: "player2").save(flush:true, failOnError: true)
        def user3 = new User(username: 'Robin',password: "player3", confirmPassword: "player3").save(flush:true, failOnError: true)
        def user4 = new User(username: 'Geoffrey',password: "player4", confirmPassword: "player4").save(flush:true, failOnError: true)

        new UserRole(user: admin, role: adminRole).save(flush:true, failOnError: true)
        new UserRole(user: user1, role: userRole).save(flush:true, failOnError: true)
        new UserRole(user: user2, role: userRole).save(flush:true, failOnError: true)
        new UserRole(user: user3, role: userRole).save(flush:true, failOnError: true)
        new UserRole(user: user4, role: userRole).save(flush:true, failOnError: true)

        new Game(winner: user1, looser: user2, winnerScore: 10, looserScore: 2).save(flush:true, failOnError: true)
        new Game(winner: user3, looser: user4, winnerScore: 15, looserScore: 5).save(flush:true, failOnError: true)
        new Game(winner: user1, looser: user4, winnerScore: 12, looserScore: 10).save(flush:true, failOnError: true)

        new Message(author: user1, target: user2, content: "I will win LOL").save(flush:true, failOnError: true)
        new Message(author: user2, target: user1, content: "Naaaaah").save(flush:true, failOnError: true)
        new Message(author: user3, target: user4, content: "How are you ???").save(flush:true, failOnError: true)
        new Message(author: user4, target: user3, content: "Fine").save(flush:true, failOnError: true)







    }
    def destroy = {
    }
}
