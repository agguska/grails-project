package fr.mbds.tp

import fr.mbds.tp.web.domain.enums.UserStatusEnum
import grails.config.Config
import grails.converters.JSON

class ApiController {

    def userManagementService
    def messageService
    def gameService
    def localeResolver
    def messageSource


    def user() {
        switch (request.getMethod()) {
            case "POST":
                def user = new User(request.JSON)

                Config config = grailsApplication.config
                def encodedFile = request.JSON.image
                if(encodedFile){
                    def filePath = config.getProperty('server.filePath')
                    def tmpFile = File.createTempFile("userImage_", ".jpg", new File(filePath))

                    FileOutputStream fos = new FileOutputStream(tmpFile)
                    fos.write(Base64.getDecoder().decode(encodedFile))
                    fos.close()

                    def fileName = tmpFile.getName()
                    user.setImageName(fileName)
                }

                if (user.save(flush: true)) {
                    def role = Role.findByAuthority(request.JSON.role)
                    def userRole = new UserRole().create(user, role, false)
                    if (userRole.save(flush: true)) {
                        render(status: 201, text: user as JSON)
                    } else {
                        this.renderErrorMessages(userRole)
                    }
                } else {
                    this.renderErrorMessages(user)
                }
                break
            case "GET":
                if (!params.id) {
                    render(status: 400, text: "Missing parameter id")
                    return
                } else if (!User.get(params.id)) {
                    render(status: 404, text: "User not found")
                    return
                } else if (UserStatusEnum.INACTIVE.equals(User.get(params.id).status)) {
                    render(status: 410, text: "User no longer available")
                    return
                }
                render User.get(params.id) as JSON
                break
            case "PUT":
                if (!params.id) {
                    render(status: 400, text: "Missing parameter id")
                    return
                } else if (!User.get(params.id)) {
                    render(status: 404, text: "User not found")
                    return
                } else if (UserStatusEnum.INACTIVE.equals(User.get(params.id).status)) {
                    render(status: 410, text: "User no longer available")
                    return
                } else {
                    def user = User.get(params.id)
                    user.properties = request.JSON
                    user.properties.confirmPassword = user.properties.password // TODO should be Command Object
                    user.merge(flush: true, failOnError: true)
                    render(status: 200, text: user as JSON)
                    return
                }
                render(status: 400, text: "Echec de la mise a jour")
                break
            case "DELETE":
                if (!params.id) {
                    render(status: 400, text: "Missing parameter id")
                    return
                } else if (!User.get(params.id)) {
                    render(status: 404, text: "User not found")
                    return
                } else if (UserStatusEnum.INACTIVE.equals(User.get(params.id).status)) {
                    render(status: 410, text: "User no longer available")
                    return
                }
                userManagementService.deleteFormUser(new Long(params.id))
                render(status: 200, text: "User successfully deleted")
                break
            default:
                response.status = 405
                break
        }
    }

    def message() {
        switch (request.getMethod()) {
            case "POST":
                def message = new Message(author: User.findByUsername(request.JSON.author),
                        target: User.findByUsername(request.JSON.target),
                        content: request.JSON.content)
                if (message.save()) {
                    render(status: 201, text: message as JSON)
                    return
                } else {
                    this.renderErrorMessages(message)
                }
                break
            case "GET":
                if (!params.id) {
                    render(status: 400, text: "Missing parameter id")
                    return
                } else if (!Message.get(params.id)) {
                    render(status: 404, text: "Message not found")
                    return
                }
                render Message.get(params.id) as JSON
                break
            case "PUT":
                if (!params.id) {
                    render(status: 400, text: "Missing parameter id")
                    return
                } else if (!Message.get(params.id)) {
                    render(status: 404, text: "Message not found")
                    return
                } else {
                    def message = Message.get(params.id)
                    message.properties = request.JSON

                    if(message.merge(flush: true, failOnError: true)) {
                        render(status: 200, text: message as JSON)
                        return
                    } else {
                        this.renderErrorMessages(message)
                    }
                }
                break
            case "DELETE":
                if (!params.id) {
                    render(status: 400, text: "Missing parameter id")
                    return
                } else if (!Message.get(params.id)) {
                    render(status: 404, text: "Message not found")
                    return
                }
                messageService.delete(params.id)
                render(status: 200, text: "Message successfully deleted")
                break
            default:
                response.status = 405
                break
        }

    }

    def game() {
        switch (request.getMethod()) {
            case "POST":
                def game = new Game(winner: User.findByUsername(request.JSON.winner),
                        looser: User.findByUsername(request.JSON.looser),
                        winnerScore: request.JSON.winnerScore,
                        looserScore: request.JSON.looserScore)
                if (game.save()) {
                    render(status: 201, text: game as JSON)
                    return
                } else {
                    this.renderErrorMessages(game)
                }
                break
            case "GET":
                if (!params.id) {
                    render(status: 400, text: "Missing parameter id")
                    return
                } else if (!Game.get(params.id)) {
                    render(status: 404, text: "Game not found")
                    return
                }
                render Game.get(params.id) as JSON
                break
            case "PUT":
                if (!params.id) {
                    render(status: 400, text: "Missing parameter id")
                    return
                } else if (!Game.get(params.id)) {
                    render(status: 404, text: "Game not found")
                    return
                } else {
                    def game = Game.get(params.id)
                    game.properties = request.JSON

                    if(game.merge(flush: true, failOnError: true)) {
                        render(status: 200, text: game as JSON)
                        return
                    } else {
                        this.renderErrorMessages(game)
                    }
                }
                break
            case "DELETE":
                if (!params.id) {
                    render(status: 400, text: "Missing parameter id")
                    return
                } else if (!Game.get(params.id)) {
                    render(status: 404, text: "Game not found")
                    return
                }
                gameService.delete(params.id)
                render(status: 200, text: "Game successfully deleted")
                break
            default:
                response.status = 405
                break
        }

    }

    def users() {
        switch (request.getMethod()) {
            case "GET":
                def userList = User.findAllByStatus(UserStatusEnum.ACTIVE)
                render(status: 200, text: userList as JSON)
                return
                break
            default:
                response.status = 405
                return
                break
        }
    }

    def messages() {
        switch (request.getMethod()) {
            case "GET":
                def messageList = Message.list()
                render(status: 200, text: messageList as JSON)
                return
                break
            default:
                response.status = 405
                break
        }
    }

    def games() {
        switch (request.getMethod()) {
            case "GET":
                def gameList = Game.list()
                render(status: 200, text: gameList as JSON)
                return
                break
            default:
                response.status = 405
                break
        }
    }

    def renderErrorMessages(Object object) {
        Locale locale = localeResolver.resolveLocale(request)
        List allErrorMessages = object.errors.allErrors.collect {
            messageSource.getMessage(it, locale)
        }
        render(status: 400, text: "Failed to save " + object.getClass().getSimpleName().toLowerCase() + " : ${allErrorMessages}")
    }
}
