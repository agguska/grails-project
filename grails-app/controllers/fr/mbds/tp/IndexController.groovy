package fr.mbds.tp

import fr.mbds.tp.Game

class IndexController {

    def index() {
        render (view: "/index/index", model: [gameList : Game.getAll()])

    }
}
