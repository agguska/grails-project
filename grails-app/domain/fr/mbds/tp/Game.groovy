package fr.mbds.tp

class Game {

    User winner
    User looser
    Integer winnerScore = 0
    Integer looserScore = 0

    Date dateCreated

    static constraints = {
        winner nullable: false
        looser nullable: false
        winnerScore nullable: false
        looserScore nullable: false
    }
}
