package fr.mbds.tp

class Message {

    User author
    User target
    String content
    Boolean messageRead = Boolean.FALSE

    Date dateCreated

    static constraints = {
        author nullable: false
        target nullable: false
        content blank: false
    }
}
