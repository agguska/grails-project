package fr.mbds.tp

import fr.mbds.tp.web.domain.enums.UserStatusEnum
import grails.compiler.GrailsCompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

import javax.persistence.EnumType
import javax.persistence.Enumerated

@GrailsCompileStatic
@EqualsAndHashCode(includes='username')
@ToString(includes='username', includeNames=true, includePackage=false)
class User implements Serializable {

    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired
    @Enumerated(EnumType.STRING)
    UserStatusEnum status = UserStatusEnum.ACTIVE
    String imageName

    static transients = ['confirmPassword']

    private static final long serialVersionUID = 1

    String username
    String password
    String confirmPassword

    String getUsername() {
        return username
    }

    void setUsername(String username) {
        this.username = username
    }

    String getPassword() {
        return password
    }

    void setPassword(String password) {
        this.password = password
    }

    String getConfirmPassword() {
        return confirmPassword
    }

    void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword
    }

    boolean getEnabled() {
        return enabled
    }

    void setEnabled(boolean enabled) {
        this.enabled = enabled
    }

    boolean getAccountExpired() {
        return accountExpired
    }

    void setAccountExpired(boolean accountExpired) {
        this.accountExpired = accountExpired
    }

    boolean getAccountLocked() {
        return accountLocked
    }

    void setAccountLocked(boolean accountLocked) {
        this.accountLocked = accountLocked
    }

    boolean getPasswordExpired() {
        return passwordExpired
    }

    void setPasswordExpired(boolean passwordExpired) {
        this.passwordExpired = passwordExpired
    }

    UserStatusEnum getStatus() {
        return status
    }

    void setStatus(UserStatusEnum status) {
        this.status = status
    }

    Set<Role> getAuthorities() {
        (UserRole.findAllByUser(this) as List<UserRole>)*.role as Set<Role>
    }

    static constraints = {
        username nullable: false, blank: false, unique: true
        password nullable: false, blank: false, password: true
        imageName nullable: true
        confirmPassword nullable: false, blank: false, password: true,
                bindable: true,
                validator: {String val, User obj ->
                    if (!obj.confirmPassword.equals(val)) {
                        "user.confirmPassword.noMatch"
                    }
                }
    }

    static mapping = {
	    password column: '`password`'
    }
}
