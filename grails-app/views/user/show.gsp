<%@ page import="fr.mbds.tp.Role" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-user" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <fieldset class="form">
                <div class="fieldcontain">
                    <label>User Game ID : </label> ${pair[0].id}
                </div>
                <div class="fieldcontain">
                    <label>Username : </label> <a href="/mbdstp/user/show/${pair[0].id}">${pair[0].username}</a>
                </div>
                <div class="fieldcontain">
                    <label>Role : </label> ${pair[1].authority}
                </div>
                <div class="fieldcontain">
                    <label>Password Expired : </label> ${pair[0].passwordExpired}
                </div>
                <div class="fieldcontain">
                    <label>Account Locked : </label> ${pair[0].accountLocked}
                </div>
                <div class="fieldcontain">
                    <label>Account Expired : </label> ${pair[0].accountExpired}
                </div>
                <div class="fieldcontain">
                    <label>Enabled : </label> ${pair[0].enabled}
                </div>
                <image src="${pair[2]}" style="width: 100px; height: 100px" alt="User photo not uploaded"/>
            </fieldset>
            <g:form resource="${pair[0]}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${pair[0]}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
