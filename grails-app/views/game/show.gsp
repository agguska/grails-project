<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'game.label', default: 'Game')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-game" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-game" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <fieldset class="form">
                <div class="fieldcontain">
                    <label>Game ID : </label> ${game.id}
                </div>
                <div class="fieldcontain">
                    <label>Winner : </label> <a href="/mbdstp/user/show/${game.winner.id}">${game.winner.username}</a>
                </div>
                <div class="fieldcontain">
                    <label>Winner Score : </label> ${game.winnerScore}
                </div>
                <div class="fieldcontain">
                    <label>Looser : </label> <a href="/mbdstp/user/show/${game.looser.id}">${game.looser.username}</a>
                </div>
                <div class="fieldcontain">
                    <label>Looser Score : </label> ${game.looserScore}
                </div>
            </fieldset>
            <g:form resource="${this.game}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.game}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
