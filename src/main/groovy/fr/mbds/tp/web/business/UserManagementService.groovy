package fr.mbds.tp.web.business

import fr.mbds.tp.Role
import fr.mbds.tp.User
import fr.mbds.tp.UserRole
import fr.mbds.tp.web.domain.enums.UserStatusEnum
import grails.config.Config
import grails.gorm.transactions.Transactional

@Transactional
class UserManagementService {

    def userService
    def userRoleService
    def grailsApplication

    def saveFormUser(User user, Map params) {

        def savedFileName = saveFile(params)
        if(savedFileName != null) {
            user.imageName = savedFileName
        } else {
            println("User image not saved. Sorry...") //TODO throw an exception or notify user
        }

        userService.save(user)

        def authorities = params.list('userRole')
        authorities.each({
            def role = Role.findByAuthority(it)
            def userRole = new UserRole().create(user, role, false)
            userRoleService.save(userRole)
        })

    }

    def saveFile(Map params) {

        Config config = grailsApplication.config
        def fileToSave = params.cfile
        if(fileToSave){
            def filePath = config.getProperty('server.filePath')
            def tmpFile = File.createTempFile("userImage_", ".jpg", new File(filePath))
            fileToSave.transferTo(tmpFile)
            return tmpFile.getName()
        }
        return null
    }

    def updateFormUser(User user, Map params) {
        userService.save(user)

        def authorities = params.list('userRole')
        def roles = []
        authorities.each {
            roles << (Role.findByAuthority(it))
        }

        def dbUserRoles = UserRole.findAllByUser(user)
        def oldUserRoles = dbUserRoles.findAll{!(it.role in roles)}
        oldUserRoles.each { it.delete() }

        def dbAuthorities = user.getAuthorities()
        def newUserAuthorities = roles.findAll{!(it in dbAuthorities)}
        newUserAuthorities.each({
            new UserRole(user: user, role: it).save()
        })
    }

    def deleteFormUser(Long userId) {
        def user = User.get(userId)
        UserRole.removeAll(user)
        user.status = UserStatusEnum.INACTIVE
        user.confirmPassword = user.password //should be Command Object
        userService.save(user)
    }

    def getUserImagePath(Long userId){
        Config config = grailsApplication.config
        def baseUrl = config.getProperty('server.baseUrl')
        def user = User.get(userId)
        return  baseUrl + user.getImageName()

    }

    def getFormUser(Long userId) {
        def userImagePath = getUserImagePath(userId)
        def user = User.get(userId)
        def pair = new Tuple(user, user.getAuthorities(), userImagePath)
        return pair
    }


    def listFormUsers(Map params) {
        def users = User.findAllByStatus(UserStatusEnum.ACTIVE, params)
        List l = new ArrayList()
        users.each { user ->
            l.add(new Tuple(user, user.getAuthorities(), getUserImagePath(user.id)))
        }
        return l
    }

}
