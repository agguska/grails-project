package fr.mbds.tp.web.domain.enums

enum UserStatusEnum {
    ACTIVE, INACTIVE
}
