#ReadMe

##grails-project

Lancer l'application dans un environement de test : [grail] test run-app.

Accéder à la console d'administration par http://\[host]:\[port]/mbdstp/dbconsole (localhost et 8081 par défaut)
__Identifiant :__ test / __Mot de passe :__ test

###Identifiant 
Pour se connecter en tant qu'administrateur :
__Identifiant :__ admin / __Mot de passe :__ admin

###Sécurité :
L'ensemble de l'application est uniquement accessible à l'administrateur 
graçe à l'anotation  ___@Secured("ROLE_ADMIN")___ ajouté à tout 
les controllers de l'application.

###Requêtes REST :
Importer dans PostMan le fichier ___Grails.postman_collection.json___ (src > test > resources)
Determiner le filePath et baseUrl dans PostMan avant d'executer les requêtes REST.

###Bonus réaliser :
- Cron pour la purge des messages lus.
- L'ajout de fichiers image.